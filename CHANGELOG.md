
## 0.0.9 [01-22-2024]

* deprecate pre-built

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!7

---

## 0.0.8 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!6

---

## 0.0.7 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!5

---

## 0.0.6 [12-21-2021]

* 2021.2 Certification

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!4

---

## 0.0.5 [12-08-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!3

---

## 0.0.4 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/aggregate-functions-for-arrays!3

---

## 0.0.3 [05-14-2021]

* [LB-450] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/aggregate-functions-for-arrays!2

---

## 0.0.2 [12-23-2020]

* [LB-450] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/aggregate-functions-for-arrays!2

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
