
<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Aggregate Functions For Arrays

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This JST allows IAP users to pass in an array of numbers and run a set of mathematical [aggregate functions](https://en.wikipedia.org/wiki/Aggregate_function) on the items. Users can also use this JST on the aggregated arrays returned by the [GroupBy Property](https://gitlab.com/itentialopensource/pre-built-automations/groupby-property) pre-built. The aggregate functions supported are:

<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>No.</th>
<th>Function</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td><a href="https://en.wikipedia.org/wiki/Summation">Sum</a></td>
</tr>
<tr>
<td>2</td>
<td><a href="https://en.wikipedia.org/wiki/Product_(mathematics)">Product</a></td>
</tr>
<tr>
<td>3</td>
<td><a href="https://en.wikipedia.org/wiki/Maximum">Maximum</a></td>
</tr>
<tr>
<td>4</td>
<td><a href="https://en.wikipedia.org/wiki/Minimum">Minimum</a></td>
</tr>
<tr>
<td>5</td>
<td><a href="https://en.wikipedia.org/wiki/Range_(statistics)">Range</a></td>
</tr>
<tr>
<td>6</td>
<td><a href="https://en.wikipedia.org/wiki/Counting">Count</a></td>
</tr>
<tr>
<td>7</td>
<td><a href="https://en.wikipedia.org/wiki/Arithmetic_mean">Arithmetic Mean</a></td>
</tr>
<tr>
<td>8</td>
<td><a href="https://en.wikipedia.org/wiki/Geometric_mean">Geometric Mean</a></td>
</tr>
<tr>
<td>9</td>
<td><a href="https://en.wikipedia.org/wiki/Harmonic_mean">Harmonic Mean</a></td>
</tr>
<tr>
<td>10</td>
<td><a href="https://en.wikipedia.org/wiki/Median">Median</a></td>
</tr>
<tr>
<td>11</td>
<td><a href="https://en.wikipedia.org/wiki/Mode_(statistics)">Mode</a></td>
</tr>
<tr>
<td>12</td>
<td><a href="https://en.wikipedia.org/wiki/Standard_deviation">Standard Deviation</a></td>
</tr>
<tr>
<td>13</td>
<td><a href="https://en.wikipedia.org/wiki/Variance">Variance</a></td>
</tr>
</tbody>
</table>


The JST expects all the items in the array to be either numbers or parsable number strings (which will be converted to numbers); however, if there is a non-number/non-parsable string item, the item will simply be ignored from all calculations and returned as part of the `valuesIgnored` array. Values which are used in calculations will be returned in the `valuesUsed` array in their original forms (as strings/numbers) and in the `valuesUsed-converted` array in their converted forms (all numbers).

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform: `^2021.2.x`

## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.
  


## How to Run

Use the following to run the pre-built:

1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the section in your workflow where you would like to perform aggregations on an array and add a  `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `aggregateFunctions` (the name of the internal JST).

3. After selecting the task, the transformation dialog will display. The inputs to the JST would be the array on which the aggregate functions have to be executed.

4. The output of the JST can be used in any task in the workflow builder that comes after the `JSON Transformation` task.

5. Save your input and outputs and the task is ready to run inside of IAP.

## Attributes  

Attributes for the pre-built are outlined in the following tables.

**Input**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>groupedArray</code></td>
<td>An array of items for mathematical aggregation.</td>
<td><code>array</code></td>
</tr>
</tbody>
</table>


**Output**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>sum</code></td>
<td>Summation of the items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>product</code></td>
<td>Product of the items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>maximum</code></td>
<td>The highest value in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>minumum</code></td>
<td>The lowest value in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>range</code></td>
<td>Difference between the lowest and highest values.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>count</code></td>
<td>Number of items used for the calculations.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>arithmeticMean</code></td>
<td>Summation of all items divided by number of items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>geometricMean</code></td>
<td>The n<sup>th</sup> root of the product of the items, where n is the number of items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>harmonicMean</code></td>
<td>The reciprocal of the arithmetic mean of the reciprocals of the items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>median</code></td>
<td>Middle value of the items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>mode</code></td>
<td>The set of values that appear most often in the array.</td>
<td><code>array[numbers]</code></td>
</tr>
<tr>
<td><code>modeFrequency</code></td>
<td>The number of times each item in the <code>mode</code> array occured in the input array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>standardDeviation</code></td>
<td>Measure of the amount of variation or dispersion of the items in the array.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>variance</code></td>
<td>Measure of how far the array items are spread out from the arithmetic mean.</td>
<td><code>number</code></td>
</tr>
<tr>
<td><code>valuesIgnored</code></td>
<td>Items of the array that were either not a number or a parsable string and were ignored from all calculations.</td>
<td><code>array</code></td>
</tr>
<tr>
<td><code>valuesUsed</code></td>
<td>Items of the array that were either a number or a parsable string and were used for all calculations.</td>
<td><code>array[numbers/strings]</code></td>
</tr>
<tr>
<td><code>valuesUsed-converted</code></td>
<td>The parsed form of all items in <code>valuesUsed</code>.</td>
<td><code>array[numbers]</code></td>
</tr>
</tbody>
</table>

## Examples
Below are examples describing how the pre-built will work for different inputs.

### Example 1

**Input** 
```
[
    1,
    "2",
    "3.3",
    4.7,
    5
]
  ```

**Output**
```
{
  "valuesIgnored": [],
  "valuesUsed": [
    1,
    "2",
    "3.3",
    4.7,
    5
  ],
  "valuesUsed-converted": [
    1,
    2,
    3.3,
    4.7,
    5
  ],
  "count": 5,
  "mode": [
    1,
    2,
    5,
    3.3,
    4.7
  ],
  "modeFrequency": 1,
  "median": 3.3,
  "geometricMean": 2.7423467009186018,
  "product": 155.1,
  "harmonicMean": 2.256525154945151,
  "sum": 16,
  "maximum": 5,
  "minimum": 1,
  "arithmeticMean": 3.2,
  "standardDeviation": 1.5349267083479916,
  "variance": 2.3560000000000003,
  "range": 4
}
```
   
<hr><br>

### Example 2

**Input**
```
[
    10,
    "20.",
    "30b",
    null,
    20,
    ".40",
    false,
    [23],
    10,
    50
 ]
  ```

**Output**
```
{
  "valuesIgnored": [
    "30b",
    null,
    false,
    [23]
  ],
  "valuesUsed": [
    10,
    "20.",
    20,
    ".40",
    10,
    50
  ],
  "valuesUsed-converted": [
    10,
    20,
    20,
    0.4,
    10,
    50
  ],
  "count": 6,
  "mode": [
    10,
    20
  ],
  "modeFrequency": 2,
  "median": 15,
  "geometricMean": 9.63492483998996,
  "product": 800000,
  "harmonicMean": 2.127659574468085,
  "sum": 110.4,
  "maximum": 50,
  "minimum": 0.4,
  "arithmeticMean": 18.400000000000002,
  "standardDeviation": 15.646085772486357,
  "variance": 244.79999999999998,
  "range": 49.6
}
```
   
## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
